package com.datadriventesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import jxl.Sheet;
import jxl.Workbook;

public class FetchingValueThroughJxl {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		File f=new File("/home/aditi.mali/Documents/testdata1.xls");
		FileInputStream fis= new FileInputStream(f);
		
		Workbook book=Workbook.getWorkbook(fis);
		Sheet sh=book.getSheet("Sheet1");
		String celldata=sh.getCell(1, 2).getContents();
		System.out.println("Value Present is:" +celldata);
		
		int rowCount= sh.getRows();
		int columnsCount=sh.getColumns();
		
		for(int i=0;i<rowCount;i++)
		{
			for(int j=0;j<columnsCount;j++)
			{
				celldata=sh.getCell(j, i).getContents();
				System.out.println(celldata);
			}
		}
		
	}

}
