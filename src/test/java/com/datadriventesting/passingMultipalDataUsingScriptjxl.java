package com.datadriventesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;

public class passingMultipalDataUsingScriptjxl {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		
		
WebDriver driver =new ChromeDriver();
		
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);

		File f=new File("/home/aditi.mali/Documents/testdata1.xls");
		FileInputStream fis= new FileInputStream(f);
		
		Workbook book=Workbook.getWorkbook(fis);
		Sheet sh=book.getSheet("Sheet1");
		String celldata=sh.getCell(1, 2).getContents();
		System.out.println("Value Present is:" +celldata);
		
		int rowCount= sh.getRows();
		int columnsCount=sh.getColumns();
		for(int i=1;i<rowCount;i++)
		{
			String username=sh.getCell(0,1).getContents();
			String password=sh.getCell(1,1).getContents();
			driver.findElement(By.linkText("Log in")).click();
			driver.findElement(By.id("Email")).sendKeys(username);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.xpath("//input[@value='Log in']")).click();
		
			driver.findElement(By.linkText("Log out")).click();
		}
		
		
	
	}

}
